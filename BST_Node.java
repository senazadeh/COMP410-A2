package BST_A2;

public class BST_Node {
  String data;
  BST_Node left;
  BST_Node right;
  BST_Node parent;
  
  BST_Node(String data){ this.data=data; }

  // --- used for testing  ----------------------------------------------
  //
  // leave these 3 methods in, as is

  public String getData(){ return data; }
  public BST_Node getLeft(){ return left; }
  public BST_Node getRight(){ return right; }

  // --- end used for testing -------------------------------------------
  
  // --------------------------------------------------------------------
  // you may add any other methods you want to get the job done
  
  public int getHeight(BST_Node node) {
	  if (node != null) {
		  int leftHeight = getHeight(node.left);
		  int rightHeight = getHeight(node.right);
		  return Math.max(leftHeight,rightHeight) + 1;
	  } else {
		  return 0;
	  }
  }
  
  // --------------------------------------------------------------------
  
  public String toString(){
    return "Data: "+this.data+", Left: "+((this.left!=null)?left.data:"null")
            +",Right: "+((this.right!=null)?right.data:"null");
  }
}
