package BST_A2;

public class BST implements BST_Interface {
  public BST_Node root;
  int size;
  
  public BST() { size = 0; root = null; }
  
  //used for testing, please leave as is
  public BST_Node getRoot() { return root; }

//  insert:
//	   in: a string (the element to be stored into the tree)
//	   return: boolean, return true if insert is successful, false otherwise
//	   effect: if the string is already in the tree, then there is no change to
//	              the tree state, and return false
//	   		   if the string is not already in the tree, then a new tree cell/node
//	              is created, the string put into it as data, the new node is linked
//	              into the tree at the proper place; size is incremented by 1,
//	              and return a true
  
  	public boolean insert(String s) {
  		
  		if (contains(s) == true) { return false; }
  		
  		BST_Node newNode = new BST_Node(s);
  		if (root == null) { 
  			root = newNode;
  			size++;
  			return true;
  		}
  		
  		BST_Node current = root;
  		while (true) {
			if (s.compareTo(current.data) < 0) {
				if (current.left == null) {
					current.left = newNode;
					newNode.parent = current;
					size++;
					return true;
				}
				current = current.left;
			}
			if (s.compareTo(current.data) > 0) {
				if (current.right == null) {
					current.right = newNode;
					newNode.parent = current;
					size++;
					return true;
				}
				current = current.right;
			}
  		}
  	}

//	remove:
//    	in: a string (the element to be taken out of the tree)
//    	return: boolean, return true if the remove is successful, false otherwise
//            this means return false if the tree size is 0
//    	effect: if the element being looked for is in the tree, unlink the node containing
//              it and return true (success); size decreases by one
//            if the element being looked for is not in the tree, return false and
//              make no change to the tree state

	public boolean remove(String s) {
  
		if (contains(s) == false) { return false; }
		if (size == 0) { return false; }
		if (s == null) { return false; }
	
		BST_Node current = root;
		
		while (true) {
			if (s.compareTo(current.data) < 0) {
				current = current.left;
			} else if (s.compareTo(current.data) > 0) {
				current = current.right;
			} else {
				break;
			}
		}
		
		if (current == root) {
			if (root.left == null && root.right == null) {
				root = null;
				size = 0;
				return true;
			} else if (root.left != null && root.right == null) {
				root.data = root.left.data;
				root.left = null;
				size = 1;
				return true;
			} else if (root.left == null && root.right != null) {
				root.data = root.right.data;
				root.right = null;
				size = 1;
				return true;
			} else {
				BST_Node replacer = current.right;
				while (replacer.left != null) {
					replacer = replacer.left;
				}
				current.data = replacer.data;
				if (replacer.parent.left == replacer) {
					if (replacer.right != null) {
						replacer.parent.left = replacer.right;
						replacer.right.parent = replacer.parent;
					} else {
						replacer.parent.left = null;
					}
				}
				if (replacer.parent.right == replacer) {
					replacer.parent.right = null;
				}
				size--;
				return true;
			}

		}
		
		if (current.left == null && current.right == null) {
			if (current.parent.left == current) {
				current.parent.left = null;
			} else {
				current.parent.right = null;
			}
			size--;
			return true;
		} else if (current.left != null && current.right == null) {
			if (current.parent.left == current) {
				current.parent.left = current.left;
				current.left.parent = current.parent;
			} else {
				current.parent.right = current.left;
				current.left.parent = current.parent;
			}
			size--;
			return true;
		} else if (current.left == null && current.right != null) {
			if (current.parent.left == current) {
				current.parent.left = current.right;
				current.right.parent = current.parent;
			} else {
				current.parent.right = current.right;
				current.right.parent = current.parent;
			}
			size--;
			return true;
		} else {
			BST_Node replacer = current.right;
			while (replacer.left != null) {
				replacer = replacer.left;
			}
			current.data = replacer.data;
			if (replacer.parent.left == replacer) {
				if (replacer.right != null) {
					replacer.parent.left = replacer.right;
					replacer.right.parent = replacer.parent;
				} else {
					replacer.parent.left = null;
				}
			}
			if (replacer.parent.right == replacer) {
				replacer.parent.right = null;
			}
			size--;
			return true;
		}
	}

//	findMin:
//		 in: none
//		 return: string, the element value from the tree that is smallest
//		 effect: no tree state change
//		 error: is tree size is 0, return null

	public String findMin() {
		if (size == 0) { return null; }

		BST_Node current = root;		
		while (current.left != null) {
			current = current.left;
		}
		return current.data;
	}

//	findMax:
//		in: none
//		return: string, the element value from the tree that is largest
//		effect: no tree state change
//		error: is tree size is 0, return null
	
	public String findMax() {
		if (size == 0) { return null; }
		
		BST_Node current = root;		
		while (current.right != null) {
			current = current.right;
		}
		return current.data;
	}

//	 empty:
//		in: nothing
//		return: boolean, true if the tree has no elements in it, true if size is 0
//		        return false otherwise
//		effect: no change to state of tree nodes 
	
	public boolean empty() {
		if (size == 0) { return true; }
		return false;
	}
 
//	contains:
//    	in: a string (the element to be seaarched for)
//    	return: boolean, return true if the string being looked for is in the tree;
//            return false otherwise
//            this means return false if tree size is 0
//    	effect: no change to the state of the tree

	public boolean contains(String s) {
		if (size == 0) { return false; }
		
		BST_Node current = root;
		while (true) {
			if (s.compareTo(current.data) == 0) {
				return true;
			}
			if (s.compareTo(current.data) < 0) {
				if (current.left == null) { return false; }
				current = current.left;
			}
			if (s.compareTo(current.data) > 0) {
				if (current.right == null) { return false; }
				current = current.right;
			}
		}
	}

//	size:
//		in: nothing
//		return: number of elements stored in the tree
//		effect: no change to state of tree nodes
		    
	public int size() {
		return size;
	}

//	  height:
//		in: none
//		return: integer, the length of the longest path in the tree from root to a leaf
//		effect: no change in tree state
//		error: return -1 is tree is empty (size is 0, root is null)
	
	public int height() {
		if (size == 0) { return -1; }
		return root.getHeight(root) - 1;
	}
	
  //--------------------------------------------------
  //
  // of course, fill in the methods implementations
  // for the interface
  //
  //--------------------------------------------------

}
